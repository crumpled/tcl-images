$( document ).ready(function() {
  //UI Setup
  $('#tabs').tabs();

  var socket = io();
  // var text = $('textarea#consoler');
  var urlInput = $('#urlInput');
  var canvas = document.getElementById('theCanvas').getContext('2d');
  var canvasCorrected = document.getElementById('canvasCorrected').getContext('2d');
  var textCanvas = document.getElementById("textCanvas").getContext("2d");
  var brightness = $('#brightness');

  // takes color information in the jQueryColorPicker's value's schema, and socket emits the color to node.
  var emitColor = function(color){
    var rgb = {
      r: Math.floor(color._r),
      g: Math.floor(color._g),
      b: Math.floor(color._b)};
    socket.emit('sendColor', {color: rgb, pixelCount: parseInt($('#pixelCount').val()|| 100,10)});
  };

  // Adds text to the canvas and emits it.
  var emitText = function(canvasContext,color,bgColor,font,delay,message){
    console.log(arguments);
    var w = canvasContext.canvas.width;
    var h = canvasContext.canvas.height;
    var color = color || "white";
    var bgColor = bgColor || "black";
    var font = font || 'Open Sans Condensed';
    var delay = delay || 500;
    var x = 0 + w;
    //this could be more sophisticated(?), we're just going to give the text a lot of room
    var widthIndex = Math.floor(((h / 2.7) * message.length) + w); // an ascii character, on average, has an aspect ratio of 1 : 2.7 :-/
    var emitAndScroll = function(){
      _.delay(function () {
        canvasContext.fillStyle = bgColor;
        canvasContext.fillRect(0, 0, w, h);
        canvasContext.fillStyle = color;
        canvasContext.font = '' + (h - 2) + 'px  "'+ font + '"';
        canvasContext.fillText(_.escape(message), x, h - 2);
        --widthIndex;
        --x;
        emitFromCanvas(canvasContext);
        if (widthIndex >= 0){
          emitAndScroll();
        }
      }, delay);
    };
    emitAndScroll();
  };

  var changeMyValue = function (color) {
    var color = 'rgb('+Math.floor(color._r)+', '+Math.floor(color._g)+', '+Math.floor(color._b)+')';
    $(this).attr('data-color', color);
    debugger;
    $(this).next('.sp-container').css("background", color);
  }

  var colorPicker = $('#colorPickerHolder').spectrum({
    flat: true,
    preferredFormat: 'rgb',
    move: $.throttle(256,emitColor),
    showButtons: false
  });

  var textColorPickerHolder = $('#textColorPickerHolder').spectrum({
    flat: true,
    preferredFormat: 'rgb',
    move: changeMyValue,
    showButtons: false
  });

  var textBackgroundPickerHolder = $('#textBackgroundPickerHolder').spectrum({
    flat: true,
    preferredFormat: 'rgb',
    move: changeMyValue,
    showButtons: false
  });

  var sendScrollingText = function () {
    var color = textColorPickerHolder.attr('data-color') || 'white';
    var bgColor = textBackgroundPickerHolder.attr('data-color' || 'black');
    var message = _.escape($('input#textInput').val()) || '';
    var delay = parseInt($('input#scrollDelay').val(), 10) || 500;
    //emitText(canvasContext,color,bgColor,font,delay,message);
    emitText(textCanvas, color, bgColor, null, delay, message);
  }

  var textScroller = $('button#scrollText').button().click(sendScrollingText);


  // take the color array of rgb triplets and emit it to node.
  // two pixels of red would look like [255, 0, 0, 255, 0, 0]
  var emitPicture = function (tripletArray) {
    socket.emit('sendPicture', tripletArray);
  }

  // Puts an unspecified-size image onto a whatever unspecified-size canvas.
  var loadImage = function(canvasContext, src, callback){
    var w = canvasContext.canvas.width;
    var h = canvasContext.canvas.height;
    var baseImage = new Image();
    canvasContext.fillStyle = "black";
    canvasContext.fillRect(0, 0, w, h);
    baseImage.src = src;
    baseImage.onload = function(){
      canvasContext.drawImage(baseImage, 0, 0, baseImage.height, baseImage.height, 0, 0, w, h);
      callback(canvasContext);
    }
  };

  // give a canvas, and the x,y coordinates of a pixel. returns {R,G,B,[R,G,B]}
  var getColorInfo = function(canvasContext, x, y){
    var pixel = canvasContext.getImageData(x, y, 1, 1).data;
    return {
      r:pixel[0],
      g:pixel[1],
      b:pixel[2],
      array: [pixel[0], pixel[1], pixel[2]]
    }
  };

  //  x x x  give it a canvas context, number of columns of lights, and the number of lights per column.
  // x x x   a 6, 5 setup would be physically configured like this IRL
  //  x x x  this is assuming you start the strand in the bottom left, and zig-zag it up and down.
  // x x x   for 6, 5 light pixels, offset like so, the native canvas size would/should be 6 by 10,
  //  x x x  which accounts for the blank spots checkered between your lights.
  // x x x
  //  x x x  returns an array of rgb triplets.
  // x x x
  var getCorrectPixels = function (canvasContext, columns, lightsPerColumn) {
    var canvasHeight = canvasContext.canvas.height; // only used for information, not to be relied on.
    var canvasWidth = canvasContext.canvas.width; // only used for information, not to be relied on.
    var pixelHeight = lightsPerColumn * 2;
    var columnsCounter = 0;
    var arrayOfArrays = [];
    var crawlColumn = function(colNumber){
      var isOdd = !(colNumber % 2);
      var yPos = isOdd ? pixelHeight - 1 : 0;
      var tripletArray = [];

      for(;isOdd ? yPos > 0 : yPos <= pixelHeight - 1; isOdd ? yPos = yPos-2 : yPos = yPos+2){
        tripletArray.push(getColorInfo(canvasContext, colNumber, yPos).array);
      }

      return tripletArray;
    };

    for(;columnsCounter < columns; ++columnsCounter){
      arrayOfArrays.push(crawlColumn(columnsCounter));
    }

    if (canvasHeight != pixelHeight) console.warn("canvas height is not optimal for LED configuration", canvas.canvas);
    if (canvasWidth != columns) console.warn("canvas width is not optimal for LED configuration", canvas.canvas);

    return _.flatten(arrayOfArrays, false);

  }

  var emitFromCanvas = function (canvasContext) {
    emitPicture(getCorrectPixels(canvasContext, 14, 7));
  };

  var adjustBrightness = function (canvasContext, targetCanvas, val) {
    console.log(arguments);

    // copy the canvas contents onto new canvas
    targetCanvas.putImageData(
      canvasContext.getImageData(0, 0, canvasContext.canvas.width, canvasContext.canvas.height),
    0, 0);

    targetCanvas.fillStyle = "rgba(0,0,0,"+parseInt(val, 10)/100+")";
    targetCanvas.fillRect(0, 0, targetCanvas.canvas.width, targetCanvas.canvas.height);
    emitFromCanvas(targetCanvas);
  }

  // text.on('blur', function(){
  //   socket.emit('text', text.val());
  // });

  urlInput.on('blur', function (e) {
    socket.emit('urlInput', encodeURI($(e.currentTarget).val()));
  });

  socket.on('pictureReady', function (url) {
    loadImage(canvas, '/images/downloaded/' + url, emitFromCanvas);
  });

  brightness.on('change', function(e){
    adjustBrightness(canvas, canvasCorrected, $(e.currentTarget).val());
  })

  //loadImage(canvas, '/images/samples/samples.jpg', emitFromCanvas);

});
