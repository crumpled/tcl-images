var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var request = require('request');
var fs = require('fs');
var SerialPort = require('serialport').SerialPort;
var http = require('http');
var _ = require('underscore');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var serialPortPath = process.argv[2] || 'COM9';

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(__dirname + '/public'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
    });
});



//Maybe move this later to a communications module
var serialPort = new SerialPort(serialPortPath, {
  // 115200, 57600, 38400, 19200, 9600, 4800, 2400, 1800, 1200, 600, 300, 200, 150, 134, 110, 75, or 50
  baudRate: 115200,
  dataBits: 8,
  parity: 'none',
  stopBits: 1
});

function socketFunctions(socket) {
  // TODO: This function needs refactoring, to make use of writeAndDrain below, and not have all the callbacks nested.
  function sendColor(data) {
    var pixelCount = data.pixelCount;
    var payload = [];
    //var testPayload = [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00];
    serialPort.drain(function(){
      serialPort.write("CHECK\n", function(err, results){
        console.log("CHECK\n");
        if (err) console.log('err ' + err);
        if (results) console.log('results ' + results);
        serialPort.drain(function () {
          serialPort.write("DATA " + data.pixelCount + "\n", function(err, results){
            console.log("DATA " + data.pixelCount + "\n");
            if (err) console.log('err ' + err);
            if (results) console.log('results ' + results);
            for(;pixelCount>0; --pixelCount){
              payload.push(data.color.r);
              payload.push(data.color.g);
              payload.push(data.color.b);
            }
            serialPort.drain(function(){
              serialPort.write(payload, function(err, results){
                console.log(payload);
                if (err) console.log('err ' + err);
                if (results) console.log('results ' + results);
                serialPort.drain(function () {
                  serialPort.write("\n", function(err, results){
                    console.log("\n");
                    if (err) console.log('err ' + err);
                    if (results) console.log('results ' + results);
                  });
                });
              });
            })
          });
        });
      });
    });
  };

  // Drain the Serial Port buffer, then write more data to it.
  function writeAndDrain (data, callback) {
    console.log(data);
    serialPort.write(data, function (err, results) {
      if (err) console.log('err ' + err);
      if (results) console.log('results ' + results);
      serialPort.drain(callback);
    });
  };

  function sendPicture (data, callback) {

    function finishUp(){
      writeAndDrain("\n");
    }

    function sendImageArray(){
      writeAndDrain(data, finishUp);
    };

    writeAndDrain("DATA " + data.length / 3 + "\n", sendImageArray)
  };

  function getImage (data, callback) {
    var filename = data.split("/")[data.split("/").length -1];
    var directory = './public/images/downloaded/'
    var saveAndSendImage = function(err, resp, body){
      var saveOptions = {
        encoding: null
      };
      socket.emit('pictureReady', filename);
    };
    var options = {
      method: 'GET',
      url: encodeURI(data)
    };
    request(options, saveAndSendImage).pipe(fs.createWriteStream(directory + filename));
  }

  console.log('a user connected');
  socket.on('text', function (data) {
    console.log(data);
  });
  socket.on('sendColor', sendColor);
  socket.on('sendPicture', sendPicture);
  socket.on('urlInput', getImage);
}

serialPort.on("open", function () {
  console.log('open');
  serialPort.on('data', function(data) {
    console.log('data received: ' + data);
  });
  io.on('connection', socketFunctions);
});




server.listen(80);

module.exports = app;
